#include <ESP8266WiFi.h> //more about beacon frames https://mrncciew.com/2014/10/08/802-11-mgmt-beacon-frame/
#include <FS.h>
#include <SimpleTimer.h>

// monkey patch to solve https://github.com/spacehuhn/esp8266_deauther/issues/64 without editing external libraries.
extern "C"
{
#include "user_interface.h"
  typedef void (*freedom_outside_cb_t)(uint8 status);
  int wifi_register_send_pkt_freedom_cb(freedom_outside_cb_t cb);
  void wifi_unregister_send_pkt_freedom_cb(void);
  int wifi_send_pkt_freedom(uint8 *buf, int len, bool sys_seq);
}
SimpleTimer timer;

const char BEACONID = 1;
const char MAXLINELENGTH = 26;
const int linesPerSection = 10;
const int sectionChangeTimePeriod = 1000 * 60 * 1; // 5 mins
int textOffset = 0;
int page = 0;

File textFile;

void setup()
{
  Serial.begin(115200);
  SPIFFS.begin();
  delay(500);

  wifi_set_opmode(STATION_MODE);
  wifi_promiscuous_enable(1);

  Serial.println('hello world');

  char fileName[12] = "";
  sprintf(fileName, "/text%d.txt", BEACONID);
  Serial.printf("\nOpening file %s", fileName);
  textFile = SPIFFS.open(fileName, "r");

  if (!textFile)
  {
    Serial.printf("File open failed");
  }
  else
  {
    timer.setInterval(1, broadCastTextSection);
    timer.setInterval(sectionChangeTimePeriod, incrementTextSection);
  }
}

void loop()
{
  timer.run();
}

void broadCastTextSection()
{
  int line = 0;
  textFile.seek(textOffset, SeekSet);

  while (textFile.available() && line < linesPerSection)
  {

    char lineBuffer[64] = "";
    char outputBuffer[MAXLINELENGTH + 3] = "";
    int bytesRead = 0;

    while (bytesRead < MAXLINELENGTH)
    {
      char readBuffer[MAXLINELENGTH] = "";
      // read until the next space character (or characters limit is reached)
      int maxBytes = MAXLINELENGTH - bytesRead - 1;
      int wordLength = textFile.readBytesUntil(' ', readBuffer, maxBytes);
      if (wordLength == 0)
      {
        break;
      }

      if (wordLength == maxBytes)
      {
        // possible that we hit maxBytes length, so rewind to last space
        textFile.seek((-1 * wordLength) - 1, SeekCur);
      }
      else
      {
        bytesRead += wordLength + 1; // account for space between words;
        sprintf(lineBuffer, "%s %s", lineBuffer, readBuffer);
      }
    }

    int absLine = (page * linesPerSection) + line + 1;

    sprintf(outputBuffer, "%d\u26D3%03d %s", BEACONID, absLine, lineBuffer);

    sendBeacon(outputBuffer);
    line++;
  }
}

void incrementTextSection()
{
  page++;
  textOffset = textOffset + (MAXLINELENGTH * linesPerSection);
  Serial.printf("\nOffset: %d", textOffset);
  if (textOffset >= textFile.size())
  {
    Serial.printf("\nOffset %d is greater the textfile size: %d", textOffset, textFile.size());
    textOffset = 0;
    page = 0;
  }
}

void sendBeacon(char *ssid)
{
  // Randomize channel //
  byte channel = random(1, 12);
  wifi_set_channel(channel);

  uint8_t packet[128] = {0x80, 0x00,                                            //Frame Control
                         0x00, 0x00,                                            //Duration
                         /*4*/ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,              //Destination address
                         /*10*/ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,             //Source address - overwritten later
                         /*16*/ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,             //BSSID - overwritten to the same as the source address
                         /*22*/ 0xc0, 0x6c,                                     //Seq-ctl
                                                                                //Frame body starts here
                         /*24*/ 0x83, 0x51, 0xf7, 0x8f, 0x0f, 0x00, 0x00, 0x00, //timestamp - the number of microseconds the AP has been active
                         /*32*/ 0xFF, 0x00,                                     //Beacon interval
                         /*34*/ 0x01, 0x04,                                     //Capability info
                                                                                /* SSID */
                         /*36*/ 0x00};

  int ssidLen = strlen(ssid);
  packet[37] = ssidLen;

  for (int i = 0; i < ssidLen; i++)
  {
    packet[38 + i] = ssid[i];
  }

  uint8_t postSSID[13] = {0x01, 0x08, 0x82, 0x84, 0x8b, 0x96, 0x24, 0x30, 0x48, 0x6c, //supported rate
                          0x03, 0x01, 0x04 /*DSSS (Current Channel)*/};

  for (int i = 0; i < 12; i++)
  {
    packet[38 + ssidLen + i] = postSSID[i];
  }

  packet[50 + ssidLen] = channel;

  // Randomize SRC MAC
  packet[10] = packet[16] = random(256);
  packet[11] = packet[17] = random(256);
  packet[12] = packet[18] = random(256);
  packet[13] = packet[19] = random(256);
  packet[14] = packet[20] = random(256);
  packet[15] = packet[21] = random(256);

  int packetSize = 51 + ssidLen;

  wifi_send_pkt_freedom(packet, packetSize, 0);
  wifi_send_pkt_freedom(packet, packetSize, 0);
  wifi_send_pkt_freedom(packet, packetSize, 0);
  delay(1);
}
