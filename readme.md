# Phantom Machine Eden

WiFi jamming an excerpt of the short story [Machine Eden](https://www.librarystack.org/machine-eden/) by Mark von Schlegel.

> MILTON FANGLINN: My name is Milton Fanglinn. I don’t teach, no. I work in the antiquities industry as a kind of. . . well let’s say a freelance scholar. I run a small consulting business, first out of New York but now from London, authenticating and researching the provenance of ancient objects in the field. Most of these don’t things have paper- work, or known owners, and are probably never going to be seen by the public. I have worked discreetly for many of the finest museums in the world, none of which would ever want its competitors to know I’d been there. Unfortunately, these last few decades have been great for business. I’ve learned to separate discretion from morality.
>
> APPLE JONES WORTHISON: How did you find yourself in this work? This doesn’t sound like a typical path for an archaeologist…

## Build

The code is based on the [FakeBeacon](https://github.com/markszabo/FakeBeaconESP8266) project by Mark Szabo-Simon.

Phantom Machine Eden requires the ESP8266 mini wifi module. This source code has been built for the Arduino version of the firmware. Text data may be uploaded to the MCU flash memory using the Arduino filesystem uploader plugin available at https://github.com/esp8266/arduino-esp8266fs-plugin

1. Add the esp8266 Board Manager to the Ardiono IDE. (see: https://learn.sparkfun.com/tutorials/esp8266-thing-hookup-guide/installing-the-esp8266-arduino-addon).

1. Text data is expected to reside in the `/data` directory and contain only basic alpha-numeric asci text. Line returns may have unexpected results. The beacon ID constant determines which file is broadcast, using the file name convention `text[BEACONID].txt`.

1. Upload data using `arduino-esp8266fs-plugin`

1. Build and deploy
